﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Canion : MonoBehaviour {

    public GameObject original;
    public Transform referencia;

    private Coroutine corutinita;
    private IEnumerator enumeradorcito, disparo;

	// Use this for initialization
	void Start () {

        enumeradorcito = pseudoHilo();

        //original = GameObject.Find("Bala");
        corutinita = StartCoroutine(enumeradorcito);

        // fuchi guacala
        StartCoroutine("corutina2");

        disparo = Disparar();
	}

    // Update is called once per frame
    void Update() {
        // crear un nuevo gameobject 
        // clonarlo - instantiate
        // para instanciar necesitamos un objeto base que copiar
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartCoroutine(disparo);
        }
        if (Input.GetKeyUp(KeyCode.Space)) {
            StopCoroutine(disparo);
            
        }

        if (Input.GetKeyUp(KeyCode.C)) {
            StopAllCoroutines();
        }

        if (Input.GetKeyUp(KeyCode.Z)) {
            //terminar pseudohilo
            // IEnumerator, coroutine
            StopCoroutine(enumeradorcito);
        }

        if (Input.GetKeyUp(KeyCode.X)) {
            //terminar corutina2
            StopCoroutine("corutina2");
        }

        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        transform.Translate(h * 5 * Time.deltaTime, 0, 0);
        transform.Rotate(10 * v * Time.deltaTime, 0, 0);
	}

    // corutinas
    // pseudo concurrencia
    // regla general en las repeticiones:
    // hacer las menos posibles por segundo mientras se sienta
    // bien
    IEnumerator pseudoHilo() {
        while (true) { 
            yield return new WaitForSeconds(0.5f);
            print("HOLA!");
        }
    }

    // segunda corutina para probar control
    IEnumerator corutina2() {
        while (true) {
            yield return new WaitForSeconds(0.3f);
            print("ADIOS.");
        }
    }

    IEnumerator Disparar() {
        while (true)
        {
            // instanciar bala
            Instantiate(
                original,
                referencia.position,
                referencia.rotation
                );
            yield return new WaitForSeconds(0.2f);
        }

    }
}
